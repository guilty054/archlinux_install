#Meter el iso a la usb
dd bs=4M if=/path/to/archlinux.iso of=/dev/sdx status=progress && sync

#########################  INSTALACION  ##############################

#Cargar el teclado en latin
loadkeys la-latin1

#--------
#Ahora es con  iwctl
#iwctl
#station device get-networks
#station device connect SSID

#Alternativamente iwctl --passphrase passphrase station device connect SSID

#edita esa madre con tu contraseña ....
mv wireless-wpa bedb44

#Activa el perfil que acabas de crear
netctl start bedb44

#Comprueba que tengas internaco ....
ping -c 4 www.google.com

#Use timedatectl(1) to ensure the system clock is accurate:
timedatectl set-ntp true

#Cambia la zona horaria por la de mexico
timedatectl set-timezone America/Mexico_City

#NOTA: El prompt del live es grml

#Ver particiones
lsblk

#Tengo sueño y estoy medio wey, así que particionaré con cfdisk de a puto :'v
cfdisk /dev/sda

#quedó así
#sda5 : swap
#sda6 : /
#sda7 : /home

##salte y entra de nuevo a cfdisk para comprobar que todo este en orden

#Formatear las nuevas particiones
mkswap /dev/sda5
mkfs.ext4 /dev/sda6
mkfs.ext4 /dev/sda7

#Montar las particiones
swapon /dev/sda5
mount /dev/sda6 /mnt
mkdir /mnt/home
mkdir /mnt/boot

mount /dev/sda7 /mnt/home
mount /dev/sda1 /mnt/boot


#si estás usando una imágen vieja de Archy y te aparece el error de que las llaves son inválidas o estan corrompidas ejecuta este comando
pacman -Sy archlinux-keyring

#YA NO INSTALES NADA DE IW 
#Instalar el sistema operativo (Intel)
pacstrap /mnt base base-devel networkmanager grub efibootmgr os-prober memtest86+-efi emacs nano tmux linux linux-firmware linux-headers ntfs-3g intel-ucode

#Instalar el sistema operativo (AMD)
pacstrap /mnt base base-devel networkmanager grub efibootmgr os-prober memtest86+-efi emacs nano tmux linux linux-firmware linux-headers ntfs-3g amd-ucode



#Generar el fstab
genfstab -U /mnt >> /mnt/etc/fstab

#Change root into the new system:
arch-chroot /mnt

#Establecer la zona horaria
ln -sf /usr/share/zoneinfo/America/Mexico_City /etc/localtime

#Run hwclock(8) to generate /etc/adjtime:
 hwclock --systohc

#Configurar el idioma
nano /etc/locale.gen

#Descomenta EN_US.utf-8, es_MX.UTF-8, ja_JP.UTF-8 ja_JP.EUC zh_CN.UTF-8 ko_KR.UTF-8
#Y luego ...

locale-gen

#Establecer el idioma y la configuración del teclado de la consola
echo "LANG=es_MX.UTF-8
LC_TIME=es_MX.UTF-8" > /etc/locale.conf

echo KEYMAP=la-latin1 > /etc/vconsole.conf

echo avalon > /etc/hostname
#Consider adding a matching entry to hosts(5):
#/etc/hosts
#127.0.0.1	localhost.localdomain	localhost
#::1		localhost.localdomain	localhost
#127.0.1.1	myhostname.localdomain	myhostname

#Escribe bajo Misc options ILoveCandy
#descomenta Color y ParallelDownlaods
#descomenta el repositorio de multilib
nano /etc/pacman.conf


passwd


#Crear initramfs
mkinitcpio -P linux

#####   GRUB (Para EFI)######
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub

### Si te aparece un error que dice algo como
### Could not prepare Boot variable: No space left on device
###Prueba con este comando: sudo rm /sys/firmware/efi/efivars/dump-type0-*
### Fuente: https://www.reddit.com/r/linuxquestions/comments/ro4ioy/efibootmgr_could_not_prepare_boot_variable_no/

##Recuerda desactivar el inicio rapido y la hibernacion de win2
#para que el os-prober lo detecte


# descomenta GRUB_DISABLE_OS_PROBER=false
nano /etc/default/grub

grub-mkconfig -o /boot/grub/grub.cfg

exit


umount -R /mnt


####NOTA: Por alguna razón os-prober no detecta a win2 desde el live usb, pero si te logueas ya en tu nuevo sistema archy
##### y generas de nuevo el grub (grub-mkconfig -o /boot/grub/grub.cfg), ahora si lo detecta chido
