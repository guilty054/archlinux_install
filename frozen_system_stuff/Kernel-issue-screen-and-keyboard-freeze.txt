https://wiki.gentoo.org/wiki/Ryzen#Soft_freezes_on_1st_gen_Ryzen_7
https://wiki.archlinux.org/title/Ryzen#Troubleshooting
https://bbs.archlinux.org/viewtopic.php?id=236686
https://bbs.archlinux.org/viewtopic.php?id=220716


Intento 1: 
Agregar el resultado del comando: 
echo rcu_nocbs=0-$(($(nproc)-1))

a los parámetros del kernel durante el arranque con grub 
    Editar el archivo /etc/default/grub y agregar el parametro en la variable GRUB_CMDLINE_LINUX_DEFAULT
    quedando así:
    GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet rcu_nocbs=0-15"

Regenerar grub 
    sudo grub-mkconfig -o /boot/grub/grub.cfg

Reiniciar


############################################################################################################################################################
Intento 2: 
Agregar estos parámetros al kernel
clearcpuid=514 rcu_nocbs=0-15 pci=noaer idle=nomwait

Regenerar grub 
sudo grub-mkconfig -o /boot/grub/grub.cfg

Reiniciar 




############################################################################################################################################################
Intento 3:
Lo mismo que el 2 pero desde windows vamos a cambiar el perfil desde el alienware command center a "overclock 1" a ver si eso aumenta el voltaje del cpu 
en linux también 





############################################################################################################################################################
Intento 4:
Agregar
processor.max_cstate=1 

a los parámetros del kernel 



Ve la salida de los comandos: 
sudo cat /sys/devices/system/cpu/cpu0/cpuidle/state*/name
y 
sudo ls /sys/devices/system/cpu/cpu*/cpuidle

los parametros del kernel los puedes ver así: 
cat /proc/cmdline



[root@titan moe]# cat /sys/devices/system/cpu/cpu0/cpuidle/state*/name
POLL
C1
C2
C3


3
[root@titan moe]# ls /sys/devices/system/cpu/cpu*/cpuidle
/sys/devices/system/cpu/cpu0/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu10/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu11/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu12/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu13/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu14/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu15/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu1/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu2/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu3/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu4/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu5/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu6/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu7/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu8/cpuidle:
state0  state1  state2  state3

/sys/devices/system/cpu/cpu9/cpuidle:
state0  state1  state2  state3



lscpu
