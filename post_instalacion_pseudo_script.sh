#Copiar el fichero de configuración de wifi a la carpeta correspondiente en /etc
cp bedb44 /et/netctl
netctl start bedb44
netctl enable bedb44


#Usar el buen NetworkManager
sudo netctl disable bedb44
sudo systemctl start NetworkManager

nmcli dev wifi con "bedb44" password 279212016 name wifi-bedb

##EEEEEEEEEEEEEEEEEEEEEEEEEEE  Para montar la partición de win2 ##############################
pacman -Syu ntfs-3g

#Crear usuario
useradd -m -g users -G wheel -s /bin/bash moe
passwd moe

#Use timedatectl(1) to ensure the system clock is accurate:
timedatectl set-ntp true

#Activar el grupo wheel para sudo, descomentar %wheel ALL=(ALL) wheel
EDITOR=nano visudo

#Ordenar los mirrors por velocidad para instalar paquetes más rápido
#Back up the existing /etc/pacman.d/mirrorlist:
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup

#run the following sed line to uncomment every mirror:
sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup

#Finally, rank the mirrors. Operand -n 6 means only output the 6 fastest mirrors:
rankmirrors -n 6 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist

#Se va a tardar un rato ....

################################### INSTALAR X SERVER ##########################################
pacman -Syu xorg xorg-xinit xterm xorg-twm

#Ya no instales nouvea porque con la msi y la hp verde truena, aparece una pantalla blanca y solo se ve el puntero cuando entras al sddm
#Instalar controladores de video (intel)
pacman -Syu xf86-video-intel

#Instalar controladores de video (AMD)
pacman -Syu mesa lib32-mesa  xf86-video-amdgpu vulkan-radeon




#Iniciar Xorg
cp /etc/X11/xinit/xinitrc ~/.xinitrc


#Iniciar Xorg para probar que este chido, NOTA: Lo matas con pkill -15 Xorg
#startx

#Usar teclado latinoamericano para Xorg
localectl --no-convert set-x11-keymap latam pc105

#instalar un display manager, kde, gnome y aplicaciones selectas,
#pacman -Syu sddm plasma-meta kde-applications plasma-wayland-session gnome-extra
pacman --needed -Syu sddm plasma plasma-wayland-session gnome chromium kwalletmanager kleopatra firefox 
netbeans mariadb jdk7-openjdk java-runtime-common git rsync filelight mpv amarok konsole yakuake 
wget yt-dlp dolphin dolphin-plugins okular gwenview kdeconnect sshfs cmake mysql-workbench 
ark adobe-source-han-sans-jp-fonts keepassxc xclip wl-clipboard ttf-baekmuk


#Crear directorios por defecto
xdg-user-dirs-update

#Habilitar el display manager
systemctl enable sddm




#Y luego agregas esto al fstab
# Mount internal Windows partition with linux compatible permissions, i.e. 755 for directories (dmask=022) and 644 for files (fmask=133)
UUID=01CD2ABB65E17DE0 /run/media/user1/Windows ntfs-3g uid=user1,gid=users,dmask=022,fmask=133 0 0


#NOTA: Con el comando blkid puedes ver la UUID
#Con el comando id -u moe puedes ver tu UID


###Quitarle el borde a las ventanas al maximizar
##Añade la siguiente línea al fichero /home/moe/.config/kwinrc
[Windows]
BorderlessMaximizedWindows=true


### Emparejado bluetooth sin conflictos entre Windows y GNU/Linux
1. Entra a Win2, empareja el dispositivo 
2. Entra a Linux, empareja el dispositivo
3. instala el paquete chntpw - es para editar archivos de registro de Win2
4. Revisa la llave que se generó al emparejar en Linux con cat /
## Formato
## cat /var/lib/bluetooth/{{MAC_PROPIA}}/{{MAC_DISPOSITIVO_TERCERO}}/info
## Ejemplo:
## cat /var/lib/bluetooth/3C\:55\:76\:5B\:CB\:6A/14\:3F\:A6\:E6\:30\:35/info
## La llave está en la sección [LinkKey]
5. Monta Win2 y ve al directorio  /[WindowsSystemDrive]/Windows/System32/config
6. Ejecuta el comando chntpw -e SYSTEM
7. Dentro del prompt ve a cd ControlSet001\Services\BTHPORT\Parameters\Keys

> cd CurrentControlSet\Services\BTHPORT\Parameters\Keys
> # if there is no CurrentControlSet, then try ControlSet001
> # on Windows 7, "services" above is lowercased.
> ls
# shows you your Bluetooth port's MAC address
Node has 1 subkeys and 0 values
  key name
  <aa1122334455>
> cd aa1122334455  # cd into the folder
> ls  
# lists the existing devices' MAC addresses
Node has 0 subkeys and 1 values
  size     type            value name             [value if type DWORD]
    16  REG_BINARY        <001f20eb4c9a>
> hex 001f20eb4c9a
=> :00000 XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX ...ignore..chars..
# ^ the XXs are the pairing key

8. Edita la llave y setea la llave que está en el info de GNU/LInux 
para editarlo ve HELP y hay una opcion para editar, el comando es uno que dice <offset> <hexbyte> <hexbyte> 
el offset es 00 y después copias los 16 bytes de la llave en formato Hexadecimal 
Fuente: https://unix.stackexchange.com/questions/255509/bluetooth-pairing-on-dual-boot-of-windows-linux-mint-ubuntu-stop-having-to-p


###################   PERSONALIZAR EL GRUB ###########################
#Entradas adicionales para grub
echo 'menuentry "System restart" {
	echo "System rebooting..."
	reboot
}

menuentry "System shutdown" {
	echo "System shutting down..."
	halt
}' >> /etc/grub.d/40_custom


####IMAGEN DE FONDO####
#Edita el archivo /etc/default/grub
#GRUB_BACKGROUND="/boot/grub/myimage"

#Regenera el grub
grub-mkconfig -o /boot/grub/grub.cfg

#Ahorrar espacio en /boot
#Me quedo sin espacio en la partición /boot así que voy a eliminar el fallback, edita el siguiente archivo de configuraicón y comenta las líneas 
nano /etc/mkinitcpio.d/linux.preset

#Comentar 
fallback_image="/boot/initramfs-linux-fallback.img
fallback_options="-S autodetect



#Para la HP nouveau parece que ya no funciona, así que instalé los paquetes
#https://bbs.archlinux.org/viewtopic.php?id=255926
pacman -Syu nvidia nvidia-utils lib32-nvidia-utils y con eso revivio, me aparecía una pantalla blanca solo con el puntero al iniciar el sddm 

#Esta cosa es para que nvidia escuche los eventos del sistema, desisntalala si genera problemas
#Fuente https://wiki.archlinux.org/title/NVIDIA/Tips_and_tricks
sudo pacman -Syu acpid
systemctl start acpid
systemctl enable acpid
