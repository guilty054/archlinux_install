#!/bin/bash
#Montar Win2 previamtne

#Abre a las variables del sistema
chntpw -e /run/media/moe/OS/Windows/System32/config/SYSTEM

#Muevete a la información de bluetooth
#el primer directorio puede ser CurrentControlSet o ControlSet001
cd ControlSet001\Services\BTHPORT\Parameters\Keys

#Ve los valores que hay, alguno de esos es la llave del bluetooth
ls 

#Metete a la única key que hay 
cd {{MAC_PROPIA}}

#ve los valores (MAC_DISPOSITIVO_TERCERO) que hay
ls

#Ve el contenido de cada valor
hex {{MAC_DISPOSITIVO_TERCERO}}

hex ControlSet001\Services\BTHPORT\Parameters\Keys\{{MAC_PROPIA}}\{{MAC_DISPOSITIVO_TERCERO}}

#Copia el valor de la llave en la configuración de linux, sección [LinkKey]
nano /var/lib/bluetooth/{{MAC_PROPIA}}/{{MAC_DISPOSITIVO_TERCERO}}/info

## Ejemplo:
nano /var/lib/bluetooth/3C\:55\:76\:5B\:CB\:6A/14\:3F\:A6\:E6\:30\:35/info

## REINICIA BLUETOOTH
sudo systemctl restart bluetooth



#Ejecutar un comando dentro de chntpw terminal 
chntpw -e /run/media/moe/OS/Windows/System32/config/SYSTEM <<< $'hex ControlSet001\\Services\\BTHPORT\\Parameters\\Keys\\3c55765bcb6a\\143fa6e63035\nq'

