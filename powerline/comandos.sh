#!/bin/bash
#Fuente:https://youtu.be/zfm2E4E7Dok
#Este powersheel funciona con bash

#Verificar que tengas instalado python
python3 --version

#Instalar gestor de paquetes de python
sudo pacman -Syu python-pip

#Instalar powerline 
pip3 install --user powerline-status

pip3 install powerline-gitstatus

#Encontrar los archivos de configuración de powerline y copiar la ruta
find / 2> /dev/null | grep powerline-daemon

#Lo encuentra aquí 
#/home/moe/.local/bin/powerline-daemon

#buscar ahora el binding de bash 
find / 2> /dev/null | grep powerline.sh

#Lo encuentra aquí 
#/home/moe/.local/lib/python3.10/site-packages/powerline/bindings/bash/powerline.sh


#Agregar esto al .bashrc
#Powerline
export PATH="$PATH:/home/moe/.local/bin/"
export LC_ALL=es_MX.UTF-8
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
source /home/moe/.local/lib/python3.10/site-packages/powerline/bindings/bash/powerline.sh

#crear directorios de personalización
mkdir -p ~/.config/powerline/colorschemes
mkdir -p ~/.config/powerline/themes/shell

#Copiar colores por defecto
cp /home/moe/.local/lib/python3.10/site-packages/powerline/config_files/colorschemes/default.json ~/.config/powerline/colorschemes/

cp /home/moe/.local/lib/python3.10/site-packages/powerline/config_files/themes/shell/default.json ~/.config/powerline/themes/shell/

#Añadir todo esto dentro del grups del colorschemes/default.json "groups"

    "gitstatus":                 { "fg": "gray8",           "bg": "gray2", "attrs": [] },
    "gitstatus_branch":          { "fg": "gray8",           "bg": "gray2", "attrs": [] },
    "gitstatus_branch_clean":    { "fg": "green",           "bg": "gray2", "attrs": [] },
    "gitstatus_branch_dirty":    { "fg": "gray8",           "bg": "gray2", "attrs": [] },
    "gitstatus_branch_detached": { "fg": "mediumpurple",    "bg": "gray2", "attrs": [] },
    "gitstatus_tag":             { "fg": "darkcyan",        "bg": "gray2", "attrs": [] },
    "gitstatus_behind":          { "fg": "gray10",          "bg": "gray2", "attrs": [] },
    "gitstatus_ahead":           { "fg": "gray10",          "bg": "gray2", "attrs": [] },
    "gitstatus_staged":          { "fg": "green",           "bg": "gray2", "attrs": [] },
    "gitstatus_unmerged":        { "fg": "brightred",       "bg": "gray2", "attrs": [] },
    "gitstatus_changed":         { "fg": "mediumorange",    "bg": "gray2", "attrs": [] },
    "gitstatus_untracked":       { "fg": "brightestorange", "bg": "gray2", "attrs": [] },
    "gitstatus_stashed":         { "fg": "darkblue",        "bg": "gray2", "attrs": [] },
    "gitstatus:divider":         { "fg": "gray8",           "bg": "gray2", "attrs": [] }
    
    
#añadir esto dentro del themes/shell/default.json -> segments.left.
			{
			    "function": "powerline_gitstatus.gitstatus",
			    "priority": 40
			}
			
#Iniciar powerline 
source ~/.bash_profile



#########################################################################################################
[moe@titan ~]$ pip3 install --user powerline-status
error: externally-managed-environment

× This environment is externally managed
╰─> To install Python packages system-wide, try 'pacman -S
    python-xyz', where xyz is the package you are trying to
    install.
    
    If you wish to install a non-Arch-packaged Python package,
    create a virtual environment using 'python -m venv path/to/venv'.
    Then use path/to/venv/bin/python and path/to/venv/bin/pip.
    
    If you wish to install a non-Arch packaged Python application,
    it may be easiest to use 'pipx install xyz', which will manage a
    virtual environment for you. Make sure you have python-pipx
    installed via pacman.

note: If you believe this is a mistake, please contact your Python installation or OS distribution provider. You can override this, at the risk of breaking your Python installation or OS, by passing --break-system-packages.
hint: See PEP 668 for the detailed specification.




pipx install powerline-status

pipx install powerline-gitstatus