
#####Entrada para HP Recovery Manager #####
#Intento 0
menuentry 'Windows Boot Manager (en /dev/sda1)' --class windows --class os $menuentry_id_option 'osprober-efi-C2C4-A4B6' {
	insmod part_gpt
	insmod fat
	set root='hd0,gpt1'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt1 --hint-efi=hd0,gpt1 --hint-baremetal=ahci0,gpt1  C2C4-A4B6
	else
	  search --no-floppy --fs-uuid --set=root C2C4-A4B6
	fi
	chainloader /EFI/Microsoft/Boot/bootmgfw.efi
}


#Intento 1

menuentry 'HP Recovery Manager' --class windows --class os $menuentry_id_option 'osprober-efi-6EAE12BFAE12802F' {
	insmod part_gpt
	insmod ntfs
	insmod ntldr
	set root='hd0,gpt8'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt8 --hint-efi=hd0,gpt8 --hint-baremetal=ahci0,gpt8  6EAE12BFAE12802F
	else
	  search --no-floppy --fs-uuid --set=root 6EAE12BFAE12802F
	fi
	chainloader /EFI/Boot/bootx64.efi
}


#INtento 1.1
menuentry 'HP Recovery Manager' --class windows --class os $menuentry_id_option 'osprober-efi-6EAE12BFAE12802F' {
	insmod part_gpt
	insmod ntfs
#	insmod ntldr
	set root='hd0,gpt8'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt8 --hint-efi=hd0,gpt8 --hint-baremetal=ahci0,gpt8  6EAE12BFAE12802F
	else
	  search --no-floppy --fs-uuid --set=root 6EAE12BFAE12802F
	fi
	chainloader /EFI/Boot/bootx64.efi
}


#intento 2

menuentry 'HP Recovery Manager' --class windows --class os $menuentry_id_option 'osprober-efi-6EAE12BFAE12802F' {
	insmod part_gpt
	insmod ntfs
#	insmod ntldr
	set root='hd0,gpt8'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt8 --hint-efi=hd0,gpt8 --hint-baremetal=ahci0,gpt8  6EAE12BFAE12802F
	else
	  search --no-floppy --fs-uuid --set=root 6EAE12BFAE12802F
	fi
	chainloader /bootmgr.efi
}


#intento 3
menuentry "Windows 7 UEFI/GPT" {
    insmod part_gpt
    insmod search_fs_uuid
    insmod chain
    search --fs-uuid --no-floppy --set=root 6EAE12BFAE12802F
    chainloader ($root)/bootmgr.efi
}


#Intento 4
menuentry "Windows 7 UEFI/GPT" {
    insmod part_gpt
    insmod search_fs_uuid
    insmod chain
    search --fs-uuid --no-floppy --set=root 6EAE12BFAE12802F
    chainloader ($root)/EFI/Boot/bootx64.efi
}

#intento 5
menuentry "Windows 7 UEFI/GPT 5" {
    insmod part_gpt
    insmod ntfs
    insmod search_fs_uuid
    insmod chain
    search --fs-uuid --no-floppy --set=root 6EAE12BFAE12802F
    chainloader ($root)/EFI/Boot/bootx64.efi
}

#intento 6
menuentry "Windows 7 UEFI/GPT 5" {
    insmod part_gpt
    insmod ntfs
    insmod search_fs_uuid
    insmod chain
    search --fs-uuid --no-floppy --set=root 6EAE12BFAE12802F
    chainloader ($root)/bootmgr.efi
}

#intento 7
menuentry "Windows 7 UEFI/GPT 5" {
    insmod part_gpt
    insmod ntfs
    insmod search_fs_uuid
    insmod chain
    search --fs-uuid --no-floppy --set=root 6EAE12BFAE12802F
    chainloader ($root)/asdffa.efi
}

#intento 8
menuentry "Windows 7 UEFI/GPT 5" {
    insmod ntfs
    set root=(hd0,gpt8)
    chainloader (${root})/bootmgr.efi
    boot
}

#
